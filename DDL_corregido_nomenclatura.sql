CREATE TABLE ANNIO(
  ID_ANNIO SERIAL,
  NOMBRE_ANNIO VARCHAR(30) NOT NULL,
  PRIMARY KEY(ID_ANNIO)
);

CREATE TABLE CIUDAD(
  ID_CIUDAD SERIAL,
  NOMBRE_CIUDAD VARCHAR(30) NOT NULL,
  PRIMARY KEY(ID_CIUDAD)
);

CREATE TABLE SEDE(
  ID_SEDE SERIAL,
  NOMBRE_SEDE VARCHAR(100) NOT NULL,
  ID_CIUDAD INT NOT NULL,
  DIRECCION VARCHAR(100) NOT NULL,
  FOREIGN KEY (ID_CIUDAD) REFERENCES CIUDAD(ID_CIUDAD),
  PRIMARY KEY(ID_SEDE)
);

insert into sede values(1,'aca',1,'hoyito');

CREATE TABLE TELEFONOSEDE(
  ID_SEDE INT NOT NULL,
  TELEFONO INT NOT NULL,
  FOREIGN KEY (ID_SEDE) REFERENCES SEDE(ID_SEDE),
  PRIMARY KEY(ID_SEDE,TELEFONO)
);

CREATE TABLE ESTADO(
  ID_ESTADO SERIAL,
  NOMBRE_ESTADO VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY(ID_ESTADO)
);

CREATE TABLE NIVEL(
  ID_NIVEL SERIAL,
  NOMBRE_NIVEL VARCHAR(30) NOT NULL,
  PRIMARY KEY(ID_NIVEL)
);


insert into nivel values(1,'aca');

CREATE TABLE CURSO(
  ID_CURSO SERIAL,
  ID_SEDE INT NOT NULL,
  ID_NIVEL INT NOT NULL,
  NOMBRE_CURSO VARCHAR(100) NOT NULL,
  FOREIGN KEY (ID_SEDE) REFERENCES SEDE(ID_SEDE),
  FOREIGN KEY (ID_NIVEL) REFERENCES NIVEL(ID_NIVEL),
  PRIMARY KEY(ID_CURSO)
);


insert into sede values(1,1,1,'hoyito');


CREATE TABLE GRUPO(
  ID_GRUPO SERIAL,
  ID_CURSO INT NOT NULL,
  NOMBRE_GRUPO VARCHAR(30) NOT NULL,
  FOREIGN KEY (ID_CURSO) REFERENCES CURSO(ID_CURSO),
  PRIMARY KEY(ID_GRUPO)
);

CREATE TABLE JORNADA(
  ID_JORNADA SERIAL,
  NOMBRE_JORNADA VARCHAR(50) NOT NULL,
  PRIMARY KEY(ID_JORNADA)
);

CREATE TABLE GRUPANNIO(
  ID_GRUPO INT NOT NULL,
  ID_ANNIO INT NOT NULL,
  ID_JORNADA INT NOT NULL,
  FOREIGN KEY (ID_GRUPO) REFERENCES GRUPO(ID_GRUPO),
  FOREIGN KEY (ID_ANNIO) REFERENCES ANNIO(ID_ANNIO),
  FOREIGN KEY (ID_JORNADA) REFERENCES JORNADA(ID_JORNADA),
  PRIMARY KEY(ID_GRUPO, ID_ANNIO, ID_JORNADA)
);

CREATE TABLE MATERIA(
  ID_MATERIA SERIAL,
  NOMBRE_MATERIA VARCHAR(50) NOT NULL,
  PRIMARY KEY(ID_MATERIA)
);

CREATE TABLE MATERIAXCURSO(
  ID_CURSO INT NOT NULL,
  ID_MATERIA INT NOT NULL,
  FOREIGN KEY (ID_CURSO) REFERENCES CURSO(ID_CURSO),
  FOREIGN KEY (ID_MATERIA) REFERENCES MATERIA(ID_MATERIA),
  PRIMARY KEY(ID_CURSO, ID_MATERIA)
);


CREATE TABLE PERIODO(
  ID_PERIODO SERIAL,
  ID_SEDE INT NOT NULL,
  NOMBRE_PERIODO VARCHAR(50) NOT NULL,
  FOREIGN KEY (ID_SEDE) REFERENCES SEDE(ID_SEDE),
  PRIMARY KEY(ID_PERIODO)
);

CREATE TABLE TIPODEPAGO(
  ID_TIPO_DE_PAGO SERIAL,
  NOMBRE_TIPO_DE_PAGO VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY(ID_TIPO_DE_PAGO)
);


CREATE TABLE TIPOUSUARIO(
  ID_TIPO_USUARIO SERIAL,
  NOMBRE_TIPO_USUARIO VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY(ID_TIPO_USUARIO)
);

CREATE TABLE USUARIO(
  ID_USUARIO INT NOT NULL,
  ID_TIPO_USUARIO INT NOT NULL,
  NOMBRE_USUARIO VARCHAR(100) NOT NULL,
  APELLIDOS VARCHAR(100) NOT NULL,
  DOCUMENTO VARCHAR(100) NOT NULL,
  TIPO_DOCUMENTO SMALLINT NOT NULL,
  DIRECCION VARCHAR(100) NOT NULL,
  CORREO VARCHAR(100) NOT NULL,
  TELEFONO VARCHAR(30) NOT NULL,
  CLAVE VARCHAR(100) NOT NULL,
  FOREIGN KEY (ID_TIPO_USUARIO) REFERENCES TIPOUSUARIO(ID_TIPO_USUARIO),
  PRIMARY KEY(ID_USUARIO)
);

CREATE TABLE CUOTA(
  ID_CUOTA SERIAL,
  ID_TIPO_DE_PAGO INT NOT NULL,
  ID_SEDE INT NOT NULL,
  ID_USUARIO INT NOT NULL,
  ID_GRUPO INT NOT NULL,
  ID_ANNIO INT NOT NULL,
  ID_JORNADA INT NOT NULL,
  PRECIO FLOAT NOT NULL,
  ESTADO_PAGO SMALLINT DEFAULT NULL,
  FOREIGN KEY (ID_TIPO_DE_PAGO) REFERENCES TIPODEPAGO(ID_TIPO_DE_PAGO),
  FOREIGN KEY (ID_SEDE) REFERENCES SEDE(ID_SEDE),
  FOREIGN KEY (ID_USUARIO) REFERENCES USUARIO(ID_USUARIO),
  FOREIGN KEY (ID_GRUPO) REFERENCES GRUPO(ID_GRUPO),
  FOREIGN KEY (ID_ANNIO) REFERENCES ANNIO(ID_ANNIO),
  FOREIGN KEY (ID_JORNADA) REFERENCES JORNADA(ID_JORNADA),
  PRIMARY KEY(ID_CUOTA)
);

CREATE TABLE NOTA(
  ID_SEDE INT NOT NULL,
  ID_USUARIO INT NOT NULL,
  ID_GRUPO INT NOT NULL,
  ID_ANNIO INT NOT NULL,
  ID_JORNADA INT NOT NULL,
  ID_PERIODO INT NOT NULL,
  ID_MATERIA INT NOT NULL,
  ID_CURSO INT NOT NULL,
  NOTA FLOAT DEFAULT '0',
  FOREIGN KEY (ID_SEDE) REFERENCES SEDE(ID_SEDE),
  FOREIGN KEY (ID_USUARIO) REFERENCES USUARIO(ID_USUARIO),
  FOREIGN KEY (ID_GRUPO) REFERENCES GRUPO(ID_GRUPO),
  FOREIGN KEY (ID_ANNIO) REFERENCES ANNIO(ID_ANNIO),
  FOREIGN KEY (ID_JORNADA) REFERENCES JORNADA(ID_JORNADA),
  FOREIGN KEY (ID_PERIODO) REFERENCES PERIODO(ID_PERIODO),
  FOREIGN KEY (ID_MATERIA) REFERENCES MATERIA(ID_MATERIA),
  FOREIGN KEY (ID_CURSO) REFERENCES CURSO(ID_CURSO),
  PRIMARY KEY(ID_USUARIO,ID_GRUPO,ID_ANNIO,ID_JORNADA,ID_PERIODO,ID_MATERIA,ID_CURSO)
);



CREATE TABLE PARTICIPANTE(
  ID_SEDE INT NOT NULL,
  ID_USUARIO INT NOT NULL,
  ID_TIPO_USUARIO INT NOT NULL,
  ID_GRUPO INT NOT NULL,
  ID_ANNIO INT NOT NULL,
  ID_JORNADA INT NOT NULL,
  FOREIGN KEY (ID_SEDE) REFERENCES SEDE(ID_SEDE),
  FOREIGN KEY (ID_USUARIO) REFERENCES USUARIO(ID_USUARIO),
  FOREIGN KEY (ID_TIPO_USUARIO) REFERENCES TIPOUSUARIO(ID_TIPO_USUARIO),
  FOREIGN KEY (ID_GRUPO) REFERENCES GRUPO(ID_GRUPO),
  FOREIGN KEY (ID_ANNIO) REFERENCES ANNIO(ID_ANNIO),
  FOREIGN KEY (ID_JORNADA) REFERENCES JORNADA(ID_JORNADA),
  PRIMARY KEY(ID_SEDE, ID_USUARIO, ID_TIPO_USUARIO, ID_GRUPO, ID_ANNIO, ID_JORNADA)
);

CREATE TABLE USUARIOCOLEGIO(
  ID_SEDE INT NOT NULL,
  ID_USUARIO INT NOT NULL,
  ID_ESTADO INT NOT NULL,
  FOREIGN KEY (ID_SEDE) REFERENCES SEDE(ID_SEDE),
  FOREIGN KEY (ID_USUARIO) REFERENCES USUARIO(ID_USUARIO),
  FOREIGN KEY (ID_ESTADO) REFERENCES ESTADO(ID_ESTADO),
  PRIMARY KEY(ID_SEDE, ID_USUARIO, ID_ESTADO)
);

CREATE TABLE DIA(
  ID_DIA SERIAL,
  NOMBRE_DIA VARCHAR(30) NOT NULL,
  PRIMARY KEY(ID_DIA)
);

CREATE TABLE HORA(
  ID_HORA SERIAL,
  NOMBRE_HORA VARCHAR(30) NOT NULL,
  PRIMARY KEY(ID_HORA)
);

CREATE TABLE HORARIO(
  ID_DIA INT NOT NULL,
  ID_HORA INT NOT NULL,
  ID_USUARIO INT NOT NULL,
  ID_MATERIA INT NOT NULL,
  ID_ANNIO INT NOT NULL,
  ID_JORNADA INT NOT NULL,
  FOREIGN KEY (ID_DIA) REFERENCES DIA(ID_DIA),
  FOREIGN KEY (ID_HORA) REFERENCES HORA(ID_HORA),
  FOREIGN KEY (ID_USUARIO) REFERENCES USUARIO(ID_USUARIO),
  FOREIGN KEY (ID_MATERIA) REFERENCES MATERIA(ID_MATERIA),
  FOREIGN KEY (ID_ANNIO) REFERENCES ANNIO(ID_ANNIO),
  FOREIGN KEY (ID_JORNADA) REFERENCES JORNADA(ID_JORNADA),
  PRIMARY KEY(ID_DIA, ID_HORA, ID_USUARIO, ID_MATERIA, ID_ANNIO, ID_JORNADA)
);