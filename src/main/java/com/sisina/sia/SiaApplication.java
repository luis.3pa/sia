package com.sisina.sia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.sisina.sia"})
public class SiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiaApplication.class, args);
	}

}
