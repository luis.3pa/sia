package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Dia;
import com.sisina.sia.repositories.DiaRepository;

@Service
public class DiaService {
	@Autowired
	DiaRepository repository;

	public List<Dia> getAll() {
		return this.repository.findAll();
	}

	public Dia add(Dia element) {
		return this.repository.save(element);
	}

	public Optional<Dia> getById(Long idDia) {
		return this.repository.findById(idDia);
	}

	public Dia update(Dia element) {
		return this.repository.save(element);
	}

	public void deleteById(Long idDia) {
		this.repository.deleteById(idDia);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
