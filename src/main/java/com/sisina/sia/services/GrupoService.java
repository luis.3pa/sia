package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Grupo;
import com.sisina.sia.repositories.GrupoRepository;

@Service
public class GrupoService {
	@Autowired
	GrupoRepository repository;

	public List<Grupo> getAll() {
		return this.repository.findAll();
	}

	public Grupo add(Grupo element) {
		return this.repository.save(element);
	}

	public Optional<Grupo> getById(int idGrupo) {
		return this.repository.findById(idGrupo);
	}

	public Grupo update(Grupo element) {
		return this.repository.save(element);
	}

	public void deleteById(int idGrupo) {
		this.repository.deleteById(idGrupo);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
