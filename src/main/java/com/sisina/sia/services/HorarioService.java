package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Horario;
import com.sisina.sia.repositories.HorarioRepository;

@Service
public class HorarioService {
	@Autowired
	HorarioRepository repository;

	public List<Horario> getAll() {
		return this.repository.findAll();
	}

	public Horario add(Horario element) {
		return this.repository.save(element);
	}

	public Optional<Horario> getById(Long idHorario) {
		return this.repository.findById(idHorario);
	}

	public Horario update(Horario element) {
		return this.repository.save(element);
	}

	public void deleteById(Long idHorario) {
		this.repository.deleteById(idHorario);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
