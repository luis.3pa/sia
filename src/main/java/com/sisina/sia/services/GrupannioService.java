package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Grupannio;
import com.sisina.sia.repositories.GrupannioRepository;

@Service
public class GrupannioService {
	@Autowired
	GrupannioRepository repository;

	public List<Grupannio> getAll() {
		return this.repository.findAll();
	}

	public Grupannio add(Grupannio element) {
		return this.repository.save(element);
	}

	public Optional<Grupannio> getById(int idGrupannio) {
		return this.repository.findById(idGrupannio);
	}

	public Grupannio update(Grupannio element) {
		return this.repository.save(element);
	}

	public void deleteById(int idGrupannio) {
		this.repository.deleteById(idGrupannio);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
