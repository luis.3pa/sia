package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Jornada;
import com.sisina.sia.repositories.JornadaRepository;

@Service
public class JornadaService {
	@Autowired
	JornadaRepository repository;

	public List<Jornada> getAll() {
		return this.repository.findAll();
	}

	public Jornada add(Jornada element) {
		return this.repository.save(element);
	}

	public Optional<Jornada> getById(int idJornada) {
		return this.repository.findById(idJornada);
	}

	public Jornada update(Jornada element) {
		return this.repository.save(element);
	}

	public void deleteById(int idJornada) {
		this.repository.deleteById(idJornada);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
