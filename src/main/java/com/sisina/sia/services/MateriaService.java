package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Materia;
import com.sisina.sia.repositories.MateriaRepository;

@Service
public class MateriaService {
	@Autowired
	MateriaRepository repository;

	public List<Materia> getAll() {
		return this.repository.findAll();
	}

	public Materia add(Materia element) {
		return this.repository.save(element);
	}

	public Optional<Materia> getById(int idMateria) {
		return this.repository.findById(idMateria);
	}

	public Materia update(Materia element) {
		return this.repository.save(element);
	}

	public void deleteById(int idMateria) {
		this.repository.deleteById(idMateria);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
