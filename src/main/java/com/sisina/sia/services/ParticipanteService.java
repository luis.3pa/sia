package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Participante;
import com.sisina.sia.repositories.ParticipanteRepository;

@Service
public class ParticipanteService {
	@Autowired
	ParticipanteRepository repository;

	public List<Participante> getAll() {
		return this.repository.findAll();
	}

	public Participante add(Participante element) {
		return this.repository.save(element);
	}

	public Optional<Participante> getById(int idParticipante) {
		return this.repository.findById(idParticipante);
	}

	public Participante update(Participante element) {
		return this.repository.save(element);
	}

	public void deleteById(int idParticipante) {
		this.repository.deleteById(idParticipante);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
