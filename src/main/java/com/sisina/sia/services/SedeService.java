package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Sede;
import com.sisina.sia.repositories.SedeRepository;

@Service
public class SedeService {
	@Autowired
	SedeRepository repository;

	public List<Sede> getAll() {
		return this.repository.findAll();
	}

	public Sede add(Sede element) {
		return this.repository.save(element);
	}

	public Optional<Sede> getById(int idSede) {
		return this.repository.findById(idSede);
	}

	public Sede update(Sede element) {
		return this.repository.save(element);
	}

	public void deleteById(int idSede) {
		this.repository.deleteById(idSede);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
