package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Curso;
import com.sisina.sia.repositories.CursoRepository;

@Service
public class CursoService {
	@Autowired
	CursoRepository repository;

	public List<Curso> getAll() {
		return this.repository.findAll();
	}

	public Curso add(Curso element) {
		return this.repository.save(element);
	}

	public Optional<Curso> getById(int idCurso) {
		return this.repository.findById(idCurso);
	}

	public Curso update(Curso element) {
		return this.repository.save(element);
	}

	public void deleteById(int idCurso) {
		this.repository.deleteById(idCurso);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
