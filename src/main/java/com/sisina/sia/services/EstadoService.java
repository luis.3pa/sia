package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Estado;
import com.sisina.sia.repositories.EstadoRepository;

@Service
public class EstadoService {
	@Autowired
	EstadoRepository repository;

	public List<Estado> getAll() {
		return this.repository.findAll();
	}

	public Estado add(Estado element) {
		return this.repository.save(element);
	}

	public Optional<Estado> getById(int idEstado) {
		return this.repository.findById(idEstado);
	}

	public Estado update(Estado element) {
		return this.repository.save(element);
	}

	public void deleteById(int idEstado) {
		this.repository.deleteById(idEstado);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
