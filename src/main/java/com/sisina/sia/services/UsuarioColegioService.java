package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.UsuarioColegio;
import com.sisina.sia.repositories.UsuarioColegioRepository;

@Service
public class UsuarioColegioService {
	@Autowired
	UsuarioColegioRepository repository;

	public List<UsuarioColegio> getAll() {
		return this.repository.findAll();
	}

	public UsuarioColegio add(UsuarioColegio element) {
		return this.repository.save(element);
	}

	public Optional<UsuarioColegio> getById(int idUsuarioColegio) {
		return this.repository.findById(idUsuarioColegio);
	}

	public UsuarioColegio update(UsuarioColegio element) {
		return this.repository.save(element);
	}

	public void deleteById(int idUsuarioColegio) {
		this.repository.deleteById(idUsuarioColegio);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
