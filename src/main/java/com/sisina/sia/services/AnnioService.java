package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Annio;
import com.sisina.sia.repositories.AnnioRepository;

@Service
public class AnnioService {
	@Autowired
	AnnioRepository repository;

	public List<Annio> getAll() {
		return this.repository.findAll();
	}

	public Annio add(Annio element) {
		return this.repository.save(element);
	}

	public Optional<Annio> getById(int idAnnio) {
		return this.repository.findById(idAnnio);
	}

	public Annio update(Annio element) {
		return this.repository.save(element);
	}

	public void deleteById(int idAnnio) {
		this.repository.deleteById(idAnnio);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
