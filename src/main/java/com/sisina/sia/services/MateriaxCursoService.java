package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.MateriaxCurso;
import com.sisina.sia.repositories.MateriaxCursoRepository;

@Service
public class MateriaxCursoService {
	
	@Autowired
	MateriaxCursoRepository repository;

	public List<MateriaxCurso> getAll() {
		return this.repository.findAll();
	}

	public MateriaxCurso add(MateriaxCurso element) {
		return this.repository.save(element);
	}

	public Optional<MateriaxCurso> getById(int idMateriaxCurso) {
		return this.repository.findById(idMateriaxCurso);
	}

	public MateriaxCurso update(MateriaxCurso element) {
		return this.repository.save(element);
	}

	public void deleteById(int idMateriaxCurso) {
		this.repository.deleteById(idMateriaxCurso);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
