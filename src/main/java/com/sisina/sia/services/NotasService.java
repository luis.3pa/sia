package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Notas;
import com.sisina.sia.repositories.NotasRepository;

@Service
public class NotasService {
	@Autowired
	NotasRepository repository;

	public List<Notas> getAll() {
		return this.repository.findAll();
	}

	public Notas add(Notas element) {
		return this.repository.save(element);
	}

	public Optional<Notas> getById(int idNotas) {
		return this.repository.findById(idNotas);
	}

	public Notas update(Notas element) {
		return this.repository.save(element);
	}

	public void deleteById(int idNotas) {
		this.repository.deleteById(idNotas);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
