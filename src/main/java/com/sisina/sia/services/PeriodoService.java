package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Periodo;
import com.sisina.sia.repositories.PeriodoRepository;

@Service
public class PeriodoService {
	@Autowired
	PeriodoRepository repository;

	public List<Periodo> getAll() {
		return this.repository.findAll();
	}

	public Periodo add(Periodo element) {
		return this.repository.save(element);
	}

	public Optional<Periodo> getById(int idPeriodo) {
		return this.repository.findById(idPeriodo);
	}

	public Periodo update(Periodo element) {
		return this.repository.save(element);
	}

	public void deleteById(int idPeriodo) {
		this.repository.deleteById(idPeriodo);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
