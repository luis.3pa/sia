package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Hora;
import com.sisina.sia.repositories.HoraRepository;

@Service
public class HoraService {
	@Autowired
	HoraRepository repository;

	public List<Hora> getAll() {
		return this.repository.findAll();
	}

	public Hora add(Hora element) {
		return this.repository.save(element);
	}

	public Optional<Hora> getById(Long idHora) {
		return this.repository.findById(idHora);
	}

	public Hora update(Hora element) {
		return this.repository.save(element);
	}

	public void deleteById(Long idHora) {
		this.repository.deleteById(idHora);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
