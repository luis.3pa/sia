package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Ciudad;
import com.sisina.sia.repositories.CiudadRepository;

@Service
public class CiudadService {
	@Autowired
	CiudadRepository repository;

	public List<Ciudad> getAll() {
		return this.repository.findAll();
	}

	public Ciudad add(Ciudad element) {
		return this.repository.save(element);
	}

	public Optional<Ciudad> getById(int idCiudad) {
		return this.repository.findById(idCiudad);
	}

	public Ciudad update(Ciudad element) {
		return this.repository.save(element);
	}

	public void deleteById(int idCiudad) {
		this.repository.deleteById(idCiudad);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
