package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.TipoDePago;
import com.sisina.sia.repositories.TipoDePagoRepository;

@Service
public class TipoDePagoService {
	@Autowired
	TipoDePagoRepository repository;

	public List<TipoDePago> getAll() {
		return this.repository.findAll();
	}

	public TipoDePago add(TipoDePago element) {
		return this.repository.save(element);
	}

	public Optional<TipoDePago> getById(int idTipoDePago) {
		return this.repository.findById(idTipoDePago);
	}

	public TipoDePago update(TipoDePago element) {
		return this.repository.save(element);
	}

	public void deleteById(int idTipoDePago) {
		this.repository.deleteById(idTipoDePago);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
