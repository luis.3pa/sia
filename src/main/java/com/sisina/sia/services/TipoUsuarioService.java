package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.TipoUsuario;
import com.sisina.sia.repositories.TipoUsuarioRepository;

@Service
public class TipoUsuarioService {
	@Autowired
	TipoUsuarioRepository repository;

	public List<TipoUsuario> getAll() {
		return this.repository.findAll();
	}

	public TipoUsuario add(TipoUsuario element) {
		return this.repository.save(element);
	}

	public Optional<TipoUsuario> getById(Long idTipoUsuario) {
		return this.repository.findById(idTipoUsuario);
	}

	public TipoUsuario update(TipoUsuario element) {
		return this.repository.save(element);
	}

	public void deleteById(Long idTipoUsuario) {
		this.repository.deleteById(idTipoUsuario);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
