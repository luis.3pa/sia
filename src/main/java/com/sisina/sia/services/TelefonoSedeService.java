package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.TelefonoSede;
import com.sisina.sia.repositories.TelefonoSedeRepository;

@Service
public class TelefonoSedeService {
	@Autowired
	TelefonoSedeRepository repository;

	public List<TelefonoSede> getAll() {
		return this.repository.findAll();
	}

	public TelefonoSede add(TelefonoSede element) {
		return this.repository.save(element);
	}

	public Optional<TelefonoSede> getById(int idTelefonoSede) {
		return this.repository.findById(idTelefonoSede);
	}

	public TelefonoSede update(TelefonoSede element) {
		return this.repository.save(element);
	}

	public void deleteById(int idTelefonoSede) {
		this.repository.deleteById(idTelefonoSede);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
