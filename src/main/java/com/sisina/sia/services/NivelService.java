package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Nivel;
import com.sisina.sia.repositories.NivelRepository;

@Service
public class NivelService {
	@Autowired
	NivelRepository repository;

	public List<Nivel> getAll() {
		return this.repository.findAll();
	}

	public Nivel add(Nivel element) {
		return this.repository.save(element);
	}

	public Optional<Nivel> getById(int idNivel) {
		return this.repository.findById(idNivel);
	}

	public Nivel update(Nivel element) {
		return this.repository.save(element);
	}

	public void deleteById(int idNivel) {
		this.repository.deleteById(idNivel);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
