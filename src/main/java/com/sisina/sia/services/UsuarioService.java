package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Usuario;
import com.sisina.sia.repositories.UsuarioRepository;

@Service
public class UsuarioService {
	@Autowired
	UsuarioRepository repository;

	public List<Usuario> getAll() {
		return this.repository.findAll();
	}

	public Usuario add(Usuario element) {
		return this.repository.save(element);
	}

	public Optional<Usuario> getById(Long idUsuario) {
		return this.repository.findById(idUsuario);
	}

	public Usuario update(Usuario element) {
		return this.repository.save(element);
	}

	public void deleteById(Long idUsuario) {
		this.repository.deleteById(idUsuario);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
