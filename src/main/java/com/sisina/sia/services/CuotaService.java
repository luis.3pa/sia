package com.sisina.sia.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sisina.sia.entities.Cuota;
import com.sisina.sia.repositories.CuotaRepository;

@Service
public class CuotaService {
	@Autowired
	CuotaRepository repository;

	public List<Cuota> getAll() {
		return this.repository.findAll();
	}

	public Cuota add(Cuota element) {
		return this.repository.save(element);
	}

	public Optional<Cuota> getById(int idCuota) {
		return this.repository.findById(idCuota);
	}

	public Cuota update(Cuota element) {
		return this.repository.save(element);
	}

	public void deleteById(int idCuota) {
		this.repository.deleteById(idCuota);
	}

	public void deleteAll() {
		this.repository.deleteAll();
	}
}
