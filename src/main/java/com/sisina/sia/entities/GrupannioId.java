package com.sisina.sia.entities;

import java.io.Serializable;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class GrupannioId implements Serializable {

	private int grupo;
	private int annio;
	private int jornada;

}
