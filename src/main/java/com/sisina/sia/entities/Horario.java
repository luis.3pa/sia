package com.sisina.sia.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 *
 * @author Eider Lemus R
 */
@Entity
@Table(name = "horario")
@Data
@IdClass(HorarioId.class)
public class Horario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@JoinColumn(name = "id_annio")
	@ManyToOne
	private Annio annio;
	@Id
	@JoinColumn(name = "id_dia")
	@ManyToOne
	private Dia dia;
	@Id
	@JoinColumn(name = "id_hora")
	@ManyToOne
	private Hora hora;
	@Id
	@JoinColumn(name = "id_jornada")
	@ManyToOne
	private Jornada jornada;
	@Id
	@JoinColumn(name = "id_materia")
	@ManyToOne
	private Materia materia;
	@Id
	@JoinColumn(name = "id_usuario")
	@ManyToOne
	private Usuario usuario;

}
