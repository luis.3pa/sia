package com.sisina.sia.entities;

import java.io.Serializable;

import lombok.Data;

@SuppressWarnings("serial")
public @Data class MateriaxCursoId implements Serializable {

	private int curso;
	private int materia;

}
