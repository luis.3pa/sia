package com.sisina.sia.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "sede")
@Data
public class Sede implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_sede")
	private int idSede;

	@Column(name = "nombre_sede")
	private String nombreSede;

	@Column(name = "direccion")
	private String direccion;

	@JoinColumn(name = "id_ciudad")
	@ManyToOne
	private Ciudad ciudad;

}
