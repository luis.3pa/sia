package com.sisina.sia.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "tipodepago")
@Data
public class TipoDePago {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tipo_de_pago")
	private int idTipoDePago;
	@Column(name = "nombre_tipo_de_pago")
	private String nombreTipoDePago;
}
