package com.sisina.sia.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "curso")
@Data
public class Curso implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_curso")
	private int idCurso;

	@JoinColumn(name = "id_sede")
	@ManyToOne
	private Sede sede;

	@JoinColumn(name = "id_nivel")
	@ManyToOne
	private Nivel nivel;

	@Column(name = "nombre_curso")
	private String nombreCurso;

}
