package com.sisina.sia.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "cuota")
@Data
public class Cuota implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cuota")
	private int idCuota;
	@JoinColumn(name = "id_tipo_de_pago")
	@ManyToOne
	private TipoDePago tipo_de_pago;
	@JoinColumn(name = "id_sede")
	@ManyToOne
	private Sede sede;
	@JoinColumn(name = "id_usuario")
	@ManyToOne
	private Usuario usuario;
	@JoinColumn(name = "id_grupo")
	@ManyToOne
	private Grupo grupo;
	@JoinColumn(name = "id_annio")
	@ManyToOne
	private Annio annio;
	@JoinColumn(name = "id_jornada")
	@ManyToOne
	private Jornada jornada;
	@Column(name = "precio")
	private float precio;
	@Column(name = "estado_pago")
	private int estadoPago;

}
