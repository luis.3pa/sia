package com.sisina.sia.entities;

import java.io.Serializable;

import lombok.Data;

@SuppressWarnings("serial")
public @Data class NotasId implements Serializable {

	private int sede;
	private int usuario;
	private int grupo;
	private int annio;
	private int jornada;
	private int materia;
	private int periodo;
	private int curso;

}
