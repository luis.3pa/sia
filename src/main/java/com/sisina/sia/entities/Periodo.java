package com.sisina.sia.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "periodo")
@Data
public class Periodo implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_periodo")
	private int idPeriodo;
	
	@JoinColumn(name = "id_sede", referencedColumnName = "id_sede")
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Sede sede;

	@Column(name = "nombre_periodo")
	private String nombrePeriodo;

}
