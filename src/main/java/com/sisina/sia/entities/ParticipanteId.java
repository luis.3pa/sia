package com.sisina.sia.entities;

import java.io.Serializable;

import lombok.Data;


@SuppressWarnings("serial")
public @Data class ParticipanteId implements Serializable {

    private int annio;
    private int sede;
    private int tipoUsuario;
    private int jornada;
    private int grupo;
    private int usuario;


}
