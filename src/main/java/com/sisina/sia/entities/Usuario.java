package com.sisina.sia.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;


/**
 *
 * @author Eider Lemus R
 */
@Entity
@Table(name = "usuario")
@Data
public class Usuario {
    
    @Id
    @Column(name = "id_usuario")
    private Long idUsuario;
      
    @Column(name = "nombre_usuario")
    private String nombreUsuario;

    @Column(name = "apellidos")
    private String apellidos;

    @Column(name = "documento")
    private String documento;

    @Column(name = "tipo_documento")
    private short tipoDocumento;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "correo")
    private String correo;

    @Column(name = "telefono")
    private String telefono;

    @Column(name = "clave")
    private String clave;
    
    @JoinColumn(name = "id_tipo_usuario")
    @ManyToOne
    private TipoUsuario idTipoUsuario;

   
    
}
