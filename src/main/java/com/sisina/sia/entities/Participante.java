package com.sisina.sia.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;


@Entity
@Table(name = "participante")
@Data
@IdClass(ParticipanteId.class)
public class Participante {
    
    @Id
    @JoinColumn(name = "id_annio")
    @ManyToOne
    private Annio annio;
    @Id
    @JoinColumn(name = "id_sede")
    @ManyToOne
    private Sede sede;
    @Id
    @JoinColumn(name = "id_tipo_usuario")
    @ManyToOne
    private TipoUsuario tipoUsuario;
    @Id
    @JoinColumn(name = "id_jornada")
    @ManyToOne
    private Jornada jornada;
    @Id
    @JoinColumn(name = "id_grupo")
    @ManyToOne
    private Grupo grupo;
    @Id
    @JoinColumn(name = "id_usuario")
    @ManyToOne
    private Usuario usuario;


}
