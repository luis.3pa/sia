package com.sisina.sia.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "grupannio")
@Data
@IdClass(GrupannioId.class)
public class Grupannio implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@JoinColumn(name = "id_grupo")
	@ManyToOne
	private Grupo grupo;
	@Id
	@JoinColumn(name = "id_annio")
	@ManyToOne
	private Annio annio;
	@Id
	@JoinColumn(name = "id_jornada")
	@ManyToOne
	private Jornada jornada;

}
