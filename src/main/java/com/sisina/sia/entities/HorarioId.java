package com.sisina.sia.entities;

import java.io.Serializable;
import lombok.Data;


@SuppressWarnings("serial")
public @Data 
 class HorarioId implements Serializable {

    private int annio;
    private int dia;
    private int hora;
    private int jornada;
    private int materia;
    private int usuario;
    
}
