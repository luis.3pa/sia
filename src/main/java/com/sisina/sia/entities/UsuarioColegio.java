package com.sisina.sia.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
@Entity
@Table(name="usuariocolegio")
@Data
@IdClass(UsuarioColegioId.class)
public class UsuarioColegio implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@JoinColumn(name = "id_sede")
	@ManyToOne
	private Sede sede;
	@Id
	@JoinColumn(name = "id_usuario")
	@ManyToOne
	private Usuario usuario;
	@Id
	@JoinColumn(name = "id_estado")
	@ManyToOne
	private Estado estado;
}
