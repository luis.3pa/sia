package com.sisina.sia.entities;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 *
 * @author Eider Lemus R
 */
@Entity
@Table(name = "dia")
@Data
public class Dia implements Serializable {
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_dia")
    private Integer idDia;
    
    @Column(name = "nombre_dia")
    private String nombreDia;
    
 
}
