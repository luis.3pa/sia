package com.sisina.sia.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="telefonosede")
@Data
@IdClass(TelefonoSedeId.class)
public class TelefonoSede {
	@Id
	@JoinColumn(name = "id_sede")
	@ManyToOne
	private Sede sede;
	@Id
	@Column(name = "telefono")
	private int telefono;
	

}
