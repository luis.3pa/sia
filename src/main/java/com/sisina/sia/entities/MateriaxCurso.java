package com.sisina.sia.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "materiaxcurso")
@Data
@IdClass(MateriaxCursoId.class)
public class MateriaxCurso implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@JoinColumn(name = "id_Curso")
	@ManyToOne
	private Curso curso;
	@Id
	@JoinColumn(name = "id_materia")
	@ManyToOne
	private Materia materia;

}
