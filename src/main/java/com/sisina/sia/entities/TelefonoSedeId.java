package com.sisina.sia.entities;



import java.io.Serializable;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class TelefonoSedeId implements Serializable {
	
	private int sede;
	private int telefono;
	

}
