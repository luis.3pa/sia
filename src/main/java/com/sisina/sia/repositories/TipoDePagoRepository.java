package com.sisina.sia.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sisina.sia.entities.TipoDePago;

@Repository
public interface TipoDePagoRepository extends JpaRepository<TipoDePago, Integer> {

}
