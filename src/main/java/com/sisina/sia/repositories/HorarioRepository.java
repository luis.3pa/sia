package com.sisina.sia.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sisina.sia.entities.Horario;

@Repository
public interface HorarioRepository extends JpaRepository<Horario, Long> {

}
