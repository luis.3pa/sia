package com.sisina.sia.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sisina.sia.entities.Nivel;

@Repository
public interface NivelRepository extends JpaRepository<Nivel, Integer> {

}
