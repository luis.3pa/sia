package com.sisina.sia.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sisina.sia.entities.Dia;

@Repository
public interface DiaRepository extends JpaRepository<Dia, Long> {
}
