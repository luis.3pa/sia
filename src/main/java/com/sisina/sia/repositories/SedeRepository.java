package com.sisina.sia.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sisina.sia.entities.Sede;

@Repository
public interface SedeRepository extends JpaRepository<Sede, Integer> {

}
