package com.sisina.sia.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sisina.sia.entities.Grupannio;

@Repository
public interface GrupannioRepository extends JpaRepository<Grupannio, Integer> {

}
