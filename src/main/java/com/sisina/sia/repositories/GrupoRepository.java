package com.sisina.sia.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sisina.sia.entities.Grupo;

@Repository
public interface GrupoRepository extends JpaRepository<Grupo, Integer> {

}
