package com.sisina.sia.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sisina.sia.entities.Participante;

@Repository
public interface ParticipanteRepository extends JpaRepository<Participante, Integer> {

}
