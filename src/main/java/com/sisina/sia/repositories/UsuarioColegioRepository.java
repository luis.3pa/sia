package com.sisina.sia.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sisina.sia.entities.UsuarioColegio;

@Repository
public interface UsuarioColegioRepository extends JpaRepository<UsuarioColegio, Integer> {
}
