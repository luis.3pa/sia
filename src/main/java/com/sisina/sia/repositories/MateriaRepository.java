package com.sisina.sia.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sisina.sia.entities.Materia;

@Repository
public interface MateriaRepository extends JpaRepository<Materia, Integer> {

}
