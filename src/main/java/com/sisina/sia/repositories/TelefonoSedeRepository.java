package com.sisina.sia.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sisina.sia.entities.TelefonoSede;

@Repository
public interface TelefonoSedeRepository extends JpaRepository<TelefonoSede, Integer> {

}
