package com.sisina.sia.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sisina.sia.entities.Periodo;

@Repository
public interface PeriodoRepository extends JpaRepository<Periodo, Integer> {

}
