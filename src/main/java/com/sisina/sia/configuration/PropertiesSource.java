package com.sisina.sia.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:myrutas.properties")
public class PropertiesSource {

}
