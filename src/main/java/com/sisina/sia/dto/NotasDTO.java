package com.sisina.sia.dto;

import lombok.Data;

@Data
public class NotasDTO {
	private SedeDTO sede;
	private UsuarioDTO usuario;
	private GrupoDTO grupo;
	private AnnioDTO annio;
	private JornadaDTO jornada;
	private MateriaDTO materia;
	private PeriodoDTO periodo;
	private CursoDTO curso;

}
