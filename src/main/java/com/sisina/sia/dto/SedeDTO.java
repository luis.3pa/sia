package com.sisina.sia.dto;

import lombok.Data;

@Data
public class SedeDTO {

	private int idSede;
	private String nombreSede;
	private CiudadDTO ciudad;
	private String direccion;

}
