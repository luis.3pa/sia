package com.sisina.sia.dto;

import lombok.Data;

@Data
public class UsuarioDTO {

	private Long idUsuario;
	private String nombreUsuario;
	private String apellidos;
	private String documento;
	private short tipoDocumento;
	private String direccion;
	private String correo;
	private String telefono;
	private String clave;
	private TipoUsuarioDTO idTipoUsuario;

}
