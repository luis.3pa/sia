package com.sisina.sia.dto;

import lombok.Data;

@Data
public class CursoDTO {
	private int idCurso;
	private SedeDTO sede;
	private NivelDTO nivel;
	private String nombreCurso;

}
