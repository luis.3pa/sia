package com.sisina.sia.dto;

import lombok.Data;

@Data
public class HorarioDTO {

	private AnnioDTO annio;
	private DiaDTO dia;
	private HoraDTO hora;
	private JornadaDTO jornada;
	private MateriaDTO materia;
	private UsuarioDTO usuario;

}
