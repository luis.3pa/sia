package com.sisina.sia.dto;

import lombok.Data;

@Data
public class UsuarioColegioDTO {


	private int sede;
	private int usuario;
	private int estado;
}
