package com.sisina.sia.dto;

import lombok.Data;

@Data
public class TelefonoSedeDTO {

	private int sede;
	private int telefono;

}
