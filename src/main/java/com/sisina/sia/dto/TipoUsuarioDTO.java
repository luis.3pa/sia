package com.sisina.sia.dto;

import lombok.Data;

@Data
public class TipoUsuarioDTO {

	private Long idTipoUsuario;
	private String nombreTipoUsuario;

}
