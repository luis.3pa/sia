package com.sisina.sia.dto;

import lombok.Data;

@Data
public class EstadoDTO {

	private int idEstado;
	private String nombreNivel;
}