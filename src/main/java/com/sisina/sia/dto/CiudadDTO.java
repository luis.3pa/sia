package com.sisina.sia.dto;

import lombok.Data;

@Data
public class CiudadDTO {

	private Integer idCiudad;
	private String nombreCiudad;

}
