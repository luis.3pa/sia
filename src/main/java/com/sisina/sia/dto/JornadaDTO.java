package com.sisina.sia.dto;

import lombok.Data;

@Data
public class JornadaDTO {

	private int idJornada;
	private String nombreJornada;

}
