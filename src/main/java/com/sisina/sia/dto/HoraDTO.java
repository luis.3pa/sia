package com.sisina.sia.dto;

import lombok.Data;

@Data
public class HoraDTO {

	private Long idHora;
	private String nombreHora;

}
