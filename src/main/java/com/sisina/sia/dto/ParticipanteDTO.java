package com.sisina.sia.dto;

import lombok.Data;

@Data
public class ParticipanteDTO {

	private AnnioDTO annio;
	private SedeDTO sede;
	private TipoUsuarioDTO tipoUsuario;
	private JornadaDTO jornada;
	private GrupoDTO grupo;
	private UsuarioDTO usuario;

}
