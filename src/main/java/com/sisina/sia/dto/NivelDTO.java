package com.sisina.sia.dto;

import lombok.Data;

@Data
public class NivelDTO {

	private int idNivel;
	private String nombreNivel;

}
