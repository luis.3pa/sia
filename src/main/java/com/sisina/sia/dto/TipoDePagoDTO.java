package com.sisina.sia.dto;

import lombok.Data;

@Data
public class TipoDePagoDTO {

	private int idTipoDePago;
	private String nombreTipoDePago;
}
