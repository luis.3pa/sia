package com.sisina.sia.dto;

import lombok.Data;

@Data
public class GrupannioDTO  {
	private int grupo;
	private int annio;
	private int jornada;

}
