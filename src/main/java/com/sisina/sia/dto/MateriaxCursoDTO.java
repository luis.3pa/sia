package com.sisina.sia.dto;

import lombok.Data;

@Data
public class MateriaxCursoDTO {

	private CursoDTO curso;
	private MateriaDTO materia;

}
