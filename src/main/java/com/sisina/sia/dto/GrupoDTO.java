package com.sisina.sia.dto;

import lombok.Data;


@Data
public class GrupoDTO {

	private int idGrupo;
	private CursoDTO curso;
	private String nombreGrupo;

}
