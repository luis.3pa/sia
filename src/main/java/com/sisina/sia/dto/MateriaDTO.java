package com.sisina.sia.dto;

import lombok.Data;

@Data
public class MateriaDTO {

	private int idMateria;
	private String nombreMateria;

}