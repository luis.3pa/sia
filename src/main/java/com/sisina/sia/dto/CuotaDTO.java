package com.sisina.sia.dto;

import lombok.Data;


@Data
public class CuotaDTO {

	private int idCuota;
	private int tipo_de_pago;
	private int sede;
	private int usuario;
	private int grupo;
	private int annio;
	private int jornada;
	private float precio;
	private int estadoPago;

}
