package com.sisina.sia.dto;

import lombok.Data;

@Data
public class AnnioDTO  {
    private Integer idAnnio;
    private String nombreAnnio; 
}
