package com.sisina.sia.dto;

import lombok.Data;

@Data
public class DiaDTO {

	private Long idDia;
	private String nombreDia;

}
