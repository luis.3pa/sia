package com.sisina.sia.dto;

import lombok.Data;

@Data
public class PeriodoDTO {

	private int idPeriodo;
	private SedeDTO sede;
	private String nombrePeriodo;

}
