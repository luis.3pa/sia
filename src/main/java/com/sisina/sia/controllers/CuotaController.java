package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.CuotaDTO;
import com.sisina.sia.entities.Cuota;
import com.sisina.sia.services.CuotaService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Cuota")
public class CuotaController {

	@Autowired
	CuotaService service;

	ModelMapper modelMapper = new ModelMapper();
	Cuota element = new Cuota();
	
	@GetMapping(value = "/all")
	public List<Cuota> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Cuota add(@RequestBody CuotaDTO elementDTO) {
		element= modelMapper.map(elementDTO, Cuota.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Cuota update(@RequestBody CuotaDTO elementDTO) {
		element= modelMapper.map(elementDTO, Cuota.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Cuota> getId(@PathVariable("id") int idCuota) {
		return this.service.getById(idCuota);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idCuota) {
		this.service.deleteById(idCuota);
	}
}
