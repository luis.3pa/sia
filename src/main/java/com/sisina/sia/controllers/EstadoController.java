package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.EstadoDTO;
import com.sisina.sia.entities.Estado;
import com.sisina.sia.services.EstadoService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Estado")
public class EstadoController {

	@Autowired
	EstadoService service;

	ModelMapper modelMapper = new ModelMapper();
	Estado element = new Estado();
	
	@GetMapping(value = "/all")
	public List<Estado> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Estado add(@RequestBody EstadoDTO elementDTO) {
		element= modelMapper.map(elementDTO, Estado.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Estado update(@RequestBody EstadoDTO elementDTO) {
		element= modelMapper.map(elementDTO, Estado.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Estado> getId(@PathVariable("id") int idEstado) {
		return this.service.getById(idEstado);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idEstado) {
		this.service.deleteById(idEstado);
	}
}
