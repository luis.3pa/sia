package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.UsuarioColegioDTO;
import com.sisina.sia.entities.UsuarioColegio;
import com.sisina.sia.services.UsuarioColegioService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/UsuarioColegio")
public class UsuarioColegioController {

	@Autowired
	UsuarioColegioService service;

	ModelMapper modelMapper = new ModelMapper();
	UsuarioColegio element = new UsuarioColegio();
	
	@GetMapping(value = "/all")
	public List<UsuarioColegio> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public UsuarioColegio add(@RequestBody UsuarioColegioDTO elementDTO) {
		element= modelMapper.map(elementDTO, UsuarioColegio.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public UsuarioColegio update(@RequestBody UsuarioColegioDTO elementDTO) {
		element= modelMapper.map(elementDTO, UsuarioColegio.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<UsuarioColegio> getId(@PathVariable("id") int idUsuarioColegio) {
		return this.service.getById(idUsuarioColegio);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idUsuarioColegio) {
		this.service.deleteById(idUsuarioColegio);
	}
}
