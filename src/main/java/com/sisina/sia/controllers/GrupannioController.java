package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.GrupannioDTO;
import com.sisina.sia.entities.Grupannio;
import com.sisina.sia.services.GrupannioService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Grupannio")
public class GrupannioController {

	@Autowired
	GrupannioService service;

	ModelMapper modelMapper = new ModelMapper();
	Grupannio element = new Grupannio();
	
	@GetMapping(value = "/all")
	public List<Grupannio> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Grupannio add(@RequestBody GrupannioDTO elementDTO) {
		element= modelMapper.map(elementDTO, Grupannio.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Grupannio update(@RequestBody GrupannioDTO elementDTO) {
		element= modelMapper.map(elementDTO, Grupannio.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Grupannio> getId(@PathVariable("id") int idGrupannio) {
		return this.service.getById(idGrupannio);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idGrupannio) {
		this.service.deleteById(idGrupannio);
	}
}
