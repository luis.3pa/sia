package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.ParticipanteDTO;
import com.sisina.sia.entities.Participante;
import com.sisina.sia.services.ParticipanteService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Participante")
public class ParticipanteController {

	@Autowired
	ParticipanteService service;

	ModelMapper modelMapper = new ModelMapper();
	Participante element = new Participante();
	
	@GetMapping(value = "/all")
	public List<Participante> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Participante add(@RequestBody ParticipanteDTO elementDTO) {
		element= modelMapper.map(elementDTO, Participante.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Participante update(@RequestBody ParticipanteDTO elementDTO) {
		element= modelMapper.map(elementDTO, Participante.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Participante> getId(@PathVariable("id") int idParticipante) {
		return this.service.getById(idParticipante);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idParticipante) {
		this.service.deleteById(idParticipante);
	}
}
