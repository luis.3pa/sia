package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.DiaDTO;
import com.sisina.sia.entities.Dia;
import com.sisina.sia.services.DiaService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Dia")
public class DiaController {

	@Autowired
	DiaService service;

	ModelMapper modelMapper = new ModelMapper();
	Dia element = new Dia();
	
	@GetMapping(value = "/all")
	public List<Dia> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Dia add(@RequestBody DiaDTO elementDTO) {
		element= modelMapper.map(elementDTO, Dia.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Dia update(@RequestBody DiaDTO elementDTO) {
		element= modelMapper.map(elementDTO, Dia.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Dia> getId(@PathVariable("id") Long idDia) {
		return this.service.getById(idDia);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") Long idDia) {
		this.service.deleteById(idDia);
	}
}
