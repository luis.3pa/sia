package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.PeriodoDTO;
import com.sisina.sia.entities.Periodo;
import com.sisina.sia.services.PeriodoService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Periodo")
public class PeriodoController {

	@Autowired
	PeriodoService service;

	ModelMapper modelMapper = new ModelMapper();
	Periodo element = new Periodo();
	
	@GetMapping(value = "/all")
	public List<Periodo> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Periodo add(@RequestBody PeriodoDTO elementDTO) {
		element= modelMapper.map(elementDTO, Periodo.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Periodo update(@RequestBody PeriodoDTO elementDTO) {
		element= modelMapper.map(elementDTO, Periodo.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Periodo> getId(@PathVariable("id") int idPeriodo) {
		return this.service.getById(idPeriodo);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idPeriodo) {
		this.service.deleteById(idPeriodo);
	}
}
