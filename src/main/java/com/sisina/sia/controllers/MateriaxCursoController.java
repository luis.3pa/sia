package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.MateriaxCursoDTO;
import com.sisina.sia.entities.MateriaxCurso;
import com.sisina.sia.services.MateriaxCursoService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/MateriaxCurso")
public class MateriaxCursoController {

	@Autowired
	MateriaxCursoService service;

	ModelMapper modelMapper = new ModelMapper();
	MateriaxCurso element = new MateriaxCurso();
	
	@GetMapping(value = "/all")
	public List<MateriaxCurso> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public MateriaxCurso add(@RequestBody MateriaxCursoDTO elementDTO) {
		element= modelMapper.map(elementDTO, MateriaxCurso.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public MateriaxCurso update(@RequestBody MateriaxCursoDTO elementDTO) {
		element= modelMapper.map(elementDTO, MateriaxCurso.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<MateriaxCurso> getId(@PathVariable("id") int idMateriaxCurso) {
		return this.service.getById(idMateriaxCurso);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idMateriaxCurso) {
		this.service.deleteById(idMateriaxCurso);
	}
}
