package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.TipoUsuarioDTO;
import com.sisina.sia.entities.TipoUsuario;
import com.sisina.sia.services.TipoUsuarioService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/TipoUsuario")
public class TipoUsuarioController {

	@Autowired
	TipoUsuarioService service;

	ModelMapper modelMapper = new ModelMapper();
	TipoUsuario element = new TipoUsuario();
	
	@GetMapping(value = "/all")
	public List<TipoUsuario> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public TipoUsuario add(@RequestBody TipoUsuarioDTO elementDTO) {
		element= modelMapper.map(elementDTO, TipoUsuario.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public TipoUsuario update(@RequestBody TipoUsuarioDTO elementDTO) {
		element= modelMapper.map(elementDTO, TipoUsuario.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<TipoUsuario> getId(@PathVariable("id") Long idTipoUsuario) {
		return this.service.getById(idTipoUsuario);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") Long idTipoUsuario) {
		this.service.deleteById(idTipoUsuario);
	}
}
