package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.CiudadDTO;
import com.sisina.sia.entities.Ciudad;
import com.sisina.sia.services.CiudadService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Ciudad")
public class CiudadController {

	@Autowired
	CiudadService service;

	ModelMapper modelMapper = new ModelMapper();
	Ciudad element = new Ciudad();
	
	@GetMapping(value = "/all")
	public List<Ciudad> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Ciudad add(@RequestBody CiudadDTO elementDTO) {
		element= modelMapper.map(elementDTO, Ciudad.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Ciudad update(@RequestBody CiudadDTO elementDTO) {
		element= modelMapper.map(elementDTO, Ciudad.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Ciudad> getId(@PathVariable("id") int idCiudad) {
		return this.service.getById(idCiudad);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idCiudad) {
		this.service.deleteById(idCiudad);
	}
}
