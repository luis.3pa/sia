package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.MateriaDTO;
import com.sisina.sia.entities.Materia;
import com.sisina.sia.services.MateriaService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Materia")
public class MateriaController {

	@Autowired
	MateriaService service;

	ModelMapper modelMapper = new ModelMapper();
	Materia element = new Materia();
	
	@GetMapping(value = "/all")
	public List<Materia> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Materia add(@RequestBody MateriaDTO elementDTO) {
		element= modelMapper.map(elementDTO, Materia.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Materia update(@RequestBody MateriaDTO elementDTO) {
		element= modelMapper.map(elementDTO, Materia.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Materia> getId(@PathVariable("id") int idMateria) {
		return this.service.getById(idMateria);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idMateria) {
		this.service.deleteById(idMateria);
	}
}
