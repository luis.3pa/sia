package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.AnnioDTO;
import com.sisina.sia.entities.Annio;
import com.sisina.sia.services.AnnioService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Annio")
public class AnnioController {

	@Autowired
	AnnioService service;

	ModelMapper modelMapper = new ModelMapper();
	Annio element = new Annio();
	
	@GetMapping(value = "/all")
	public List<Annio> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Annio add(@RequestBody AnnioDTO elementDTO) {
		element= modelMapper.map(elementDTO, Annio.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Annio update(@RequestBody AnnioDTO elementDTO) {
		element= modelMapper.map(elementDTO, Annio.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Annio> getId(@PathVariable("id") int idAnnio) {
		return this.service.getById(idAnnio);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idAnnio) {
		this.service.deleteById(idAnnio);
	}
}
