package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.GrupoDTO;
import com.sisina.sia.entities.Grupo;
import com.sisina.sia.services.GrupoService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Grupo")
public class GrupoController {

	@Autowired
	GrupoService service;

	ModelMapper modelMapper = new ModelMapper();
	Grupo element = new Grupo();
	
	@GetMapping(value = "/all")
	public List<Grupo> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Grupo add(@RequestBody GrupoDTO elementDTO) {
		element= modelMapper.map(elementDTO, Grupo.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Grupo update(@RequestBody GrupoDTO elementDTO) {
		element= modelMapper.map(elementDTO, Grupo.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Grupo> getId(@PathVariable("id") int idGrupo) {
		return this.service.getById(idGrupo);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idGrupo) {
		this.service.deleteById(idGrupo);
	}
}
