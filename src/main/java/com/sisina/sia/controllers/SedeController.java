package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.SedeDTO;
import com.sisina.sia.entities.Sede;
import com.sisina.sia.services.CiudadService;
import com.sisina.sia.services.SedeService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Sede")
public class SedeController {

	@Autowired
	SedeService service;
	
	@Autowired
	CiudadService ciudadService;

	ModelMapper modelMapper = new ModelMapper();
	Sede element = new Sede();
	
	@GetMapping(value = "/all")
	public List<Sede> getAll() {
		return service.getAll();
	}
	
	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Sede add(@RequestBody SedeDTO elementDTO) {
		element= modelMapper.map(elementDTO, Sede.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Sede update(@RequestBody SedeDTO elementDTO) {
		element= modelMapper.map(elementDTO, Sede.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Sede> getId(@PathVariable("id") int idSede) {
		return this.service.getById(idSede);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idSede) {
		this.service.deleteById(idSede);
	}
}
