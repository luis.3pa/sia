package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.NotasDTO;
import com.sisina.sia.entities.Notas;
import com.sisina.sia.services.NotasService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Notas")
public class NotasController {

	@Autowired
	NotasService service;

	ModelMapper modelMapper = new ModelMapper();
	Notas element = new Notas();
	
	@GetMapping(value = "/all")
	public List<Notas> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Notas add(@RequestBody NotasDTO elementDTO) {
		element= modelMapper.map(elementDTO, Notas.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Notas update(@RequestBody NotasDTO elementDTO) {
		element= modelMapper.map(elementDTO, Notas.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Notas> getId(@PathVariable("id") int idNotas) {
		return this.service.getById(idNotas);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idNotas) {
		this.service.deleteById(idNotas);
	}
}
