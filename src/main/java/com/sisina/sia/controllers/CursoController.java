package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.CursoDTO;
import com.sisina.sia.entities.Curso;
import com.sisina.sia.services.CursoService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Curso")
public class CursoController {

	@Autowired
	CursoService service;

	ModelMapper modelMapper = new ModelMapper();
	Curso element = new Curso();
	
	@GetMapping(value = "/all")
	public List<Curso> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Curso add(@RequestBody CursoDTO elementDTO) {
		element= modelMapper.map(elementDTO, Curso.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Curso update(@RequestBody CursoDTO elementDTO) {
		element= modelMapper.map(elementDTO, Curso.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Curso> getId(@PathVariable("id") int idCurso) {
		return this.service.getById(idCurso);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idCurso) {
		this.service.deleteById(idCurso);
	}
}
