package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.HorarioDTO;
import com.sisina.sia.entities.Horario;
import com.sisina.sia.services.HorarioService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Horario")
public class HorarioController {

	@Autowired
	HorarioService service;

	ModelMapper modelMapper = new ModelMapper();
	Horario element = new Horario();
	
	@GetMapping(value = "/all")
	public List<Horario> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Horario add(@RequestBody HorarioDTO elementDTO) {
		element= modelMapper.map(elementDTO, Horario.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Horario update(@RequestBody HorarioDTO elementDTO) {
		element= modelMapper.map(elementDTO, Horario.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Horario> getId(@PathVariable("id") Long idHorario) {
		return this.service.getById(idHorario);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") Long idHorario) {
		this.service.deleteById(idHorario);
	}
}
