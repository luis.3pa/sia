package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.JornadaDTO;
import com.sisina.sia.entities.Jornada;
import com.sisina.sia.services.JornadaService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Jornada")
public class JornadaController {

	@Autowired
	JornadaService service;

	ModelMapper modelMapper = new ModelMapper();
	Jornada element = new Jornada();
	
	@GetMapping(value = "/all")
	public List<Jornada> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Jornada add(@RequestBody JornadaDTO elementDTO) {
		element= modelMapper.map(elementDTO, Jornada.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Jornada update(@RequestBody JornadaDTO elementDTO) {
		element= modelMapper.map(elementDTO, Jornada.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Jornada> getId(@PathVariable("id") int idJornada) {
		return this.service.getById(idJornada);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idJornada) {
		this.service.deleteById(idJornada);
	}
}
