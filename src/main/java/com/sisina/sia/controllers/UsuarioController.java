package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.UsuarioDTO;
import com.sisina.sia.entities.Usuario;
import com.sisina.sia.services.UsuarioService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Usuario")
public class UsuarioController {

	@Autowired
	UsuarioService service;

	ModelMapper modelMapper = new ModelMapper();
	Usuario element = new Usuario();
	
	@GetMapping(value = "/all")
	public List<Usuario> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Usuario add(@RequestBody UsuarioDTO elementDTO) {
		element= modelMapper.map(elementDTO, Usuario.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Usuario update(@RequestBody UsuarioDTO elementDTO) {
		element= modelMapper.map(elementDTO, Usuario.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Usuario> getId(@PathVariable("id") Long idUsuario) {
		return this.service.getById(idUsuario);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") Long idUsuario) {
		this.service.deleteById(idUsuario);
	}
}
