package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.NivelDTO;
import com.sisina.sia.entities.Nivel;
import com.sisina.sia.services.NivelService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Nivel")
public class NivelController {

	@Autowired
	NivelService service;

	ModelMapper modelMapper = new ModelMapper();
	Nivel element = new Nivel();
	
	@GetMapping(value = "/all")
	public List<Nivel> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Nivel add(@RequestBody NivelDTO elementDTO) {
		element= modelMapper.map(elementDTO, Nivel.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Nivel update(@RequestBody NivelDTO elementDTO) {
		element= modelMapper.map(elementDTO, Nivel.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Nivel> getId(@PathVariable("id") int idNivel) {
		return this.service.getById(idNivel);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idNivel) {
		this.service.deleteById(idNivel);
	}
}
