package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.TipoDePagoDTO;
import com.sisina.sia.entities.TipoDePago;
import com.sisina.sia.services.TipoDePagoService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/TipoDePago")
public class TipoDePagoController {

	@Autowired
	TipoDePagoService service;

	ModelMapper modelMapper = new ModelMapper();
	TipoDePago element = new TipoDePago();
	
	@GetMapping(value = "/all")
	public List<TipoDePago> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public TipoDePago add(@RequestBody TipoDePagoDTO elementDTO) {
		element= modelMapper.map(elementDTO, TipoDePago.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public TipoDePago update(@RequestBody TipoDePagoDTO elementDTO) {
		element= modelMapper.map(elementDTO, TipoDePago.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<TipoDePago> getId(@PathVariable("id") int idTipoDePago) {
		return this.service.getById(idTipoDePago);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idTipoDePago) {
		this.service.deleteById(idTipoDePago);
	}
}
