package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.HoraDTO;
import com.sisina.sia.entities.Hora;
import com.sisina.sia.services.HoraService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/Hora")
public class HoraController {

	@Autowired
	HoraService service;

	ModelMapper modelMapper = new ModelMapper();
	Hora element = new Hora();
	
	@GetMapping(value = "/all")
	public List<Hora> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Hora add(@RequestBody HoraDTO elementDTO) {
		element= modelMapper.map(elementDTO, Hora.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Hora update(@RequestBody HoraDTO elementDTO) {
		element= modelMapper.map(elementDTO, Hora.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<Hora> getId(@PathVariable("id") Long idHora) {
		return this.service.getById(idHora);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") Long idHora) {
		this.service.deleteById(idHora);
	}
}
