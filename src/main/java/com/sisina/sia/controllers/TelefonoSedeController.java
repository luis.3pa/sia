package com.sisina.sia.controllers;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sisina.sia.dto.TelefonoSedeDTO;
import com.sisina.sia.entities.TelefonoSede;
import com.sisina.sia.services.TelefonoSedeService;

@RestController
@CrossOrigin(origins = ("${crossOrigin}"))
@RequestMapping("/TelefonoSede")
public class TelefonoSedeController {

	@Autowired
	TelefonoSedeService service;

	ModelMapper modelMapper = new ModelMapper();
	TelefonoSede element = new TelefonoSede();
	
	@GetMapping(value = "/all")
	public List<TelefonoSede> getAll() {
		return service.getAll();
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public TelefonoSede add(@RequestBody TelefonoSedeDTO elementDTO) {
		element= modelMapper.map(elementDTO, TelefonoSede.class);
		return this.service.add(element);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public TelefonoSede update(@RequestBody TelefonoSedeDTO elementDTO) {
		element= modelMapper.map(elementDTO, TelefonoSede.class);
		return this.service.update(element);
	}

	@GetMapping(value = "/{id}")
	public Optional<TelefonoSede> getId(@PathVariable("id") int idTelefonoSede) {
		return this.service.getById(idTelefonoSede);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable("id") int idTelefonoSede) {
		this.service.deleteById(idTelefonoSede);
	}
}
